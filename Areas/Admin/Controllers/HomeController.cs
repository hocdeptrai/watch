﻿using Microsoft.AspNetCore.Mvc;

namespace ShopDongHo.Areas.Admin.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}
